import Vue from 'vue'
import App from './App.vue'

import gw from '../../npm/index'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

var ext = {
  // 心跳空闲
  beatIdle: 30000,
  // 接收超时 = 服务端IdleTime x2 或 +30s
  idleTimeout: 60000,
  // 授权数据
  loginData(ws) {
    return gw.libs.bytes.stringToBytes("" + new Date().getTime())
  },
  // 推送数据处理 !uri && !data && pid 为 pid编号消息发送失败 也作为lastId, 也作为lastId, 进度标识
  onPush(uri, data, pid) {
    console.log('onPush', arguments)
  },
  // 推送消息管道通知 gid 管道编号 connVer 推送消息时，连接版本，调用逻辑服务器Disc方法，附加验证 continues 为发送推送数据时，附加通知
  onLast(gid, connVer, continues) {
    // 可以在附加消息逻辑 检测当前gid管道 是否监听， 不监听可调用逻辑服务器Disc方法， 防止之前调用逻辑服务器Disc可以未成功的情况
    console.log('onLast', arguments)
  },
  onState(ws, state, data) {
    // 监听client连接状态编号
    /*
    gw.state
    state: {
        CONN: 0, // 开始连接
        OPEN: 1, // 连接开启
        LOOP: 2, // 可以通讯
        CLOSE: 3, // 连接关闭
        ERROR: 4, // 连接错误
        KICK: 5, // 被剔
    },
     */
    console.log('onState', arguments)
  },
  // 保留通道消息处理
  OnReserve(req, uri, uriI, data) {
  },
}
// ws://gw.dev.yiyiny.com/gw
// ws://127.0.0.1:8682/gw
var client = gw.newClient(1, "ws://127.0.0.1:8682/gw", ext)
var clientD = gw.newClient(1, "ws://gw.dev.yiyiny.com/gw", ext)

// 建立连接
// client.conn()
//  发送请求
// client.req(uri, data, function(err, data){})
global.abClients = function (num) {
  for (var i = 0; i < num; i++) {
    var client = gw.newClient(1, "ws://127.0.0.1:8682/gw", ext)
    client.conn()
  }
}

global.abReqs = function (num) {
  for (var i = 0; i < num; i++) {
    client.req('test/sendU', gw.libs.bytes.stringToBytes(JSON.stringify(["uri" + i, "data" + i])), false, 30, function (err, data) {
      console.log(err, data);
    })
  }
}

global.gw = gw
global.client = client
global.clientD = clientD