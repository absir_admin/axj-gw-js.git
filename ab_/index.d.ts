declare namespace ab_{
export const _ver: boolean;
export const local: boolean;
export const https: boolean;
export function fn(fn: any): void;
export function header(): void;
export function back(): void;
export function push(uri: any, title: any): void;
export function replace(uri: any, title: any): void;
export function open(uri: any, title: any, alone: any): void;
export function get(name: any): string;
export function empty(o: any): boolean;
export function urlRm(url: any, para: any): any;
export function urlParas(url: any, paras: any, force: any): any;
export function urlPath(): string;
export function pathDir(path: any): any;
export function route(anch: any, url: any, set: any): any;
export function el(el: any): any;
export function ver(url: any): any;
export function reqCss(href: any, el: any, media: any): void;
export function call(...args: any[]): void;
export function requires(requires: any, fun: any): void;
export function nextT(): number;
export function reqJs(js: any, el: any, onload: any, onerror: any): void;
export function reqModule(name: any, jses: any, csses: any): (call: any) => void;
export function eval(exp: any, $page: any, $this: any, res: any): void;
export const $center: {};
export function args(args: any, off: any, len: any): any;
export function swap(ary: any, from: any, to: any): any;
export function reSort(ary: any, from: any, to: any): any;
export function resetEl(children: any, fromI: any, toI: any): void;
export function swapEl(from: any, to: any): void;
export function post(name: any, ...args: any[]): void;
export function posts(data: any, i: any, from: any): void;
export const langs: {};
export function langChange(lang: any, call: any): void;
export function lang(key: any, lang: any): void;
export function i18n(key: any, par: any): any;
export function time(): number;
export function humTime(time: any, max: any): string;
export function humSize(size: any): string;
export function parseJson(json: any): any;
export function toJson(obj: any): string;
export function tplRender(tpl: any, render: any): any;
export function xhrReq(loading: any, headers: any, url: any, posts: any, callback: any, init: any): void;
export function uniReq(loading: any, headers: any, url: any, posts: any, files: any, callback: any, init: any): void;
export function newReady(last: any): {
    (back: any): void;
    start(): void;
};
export function newStart(run: any): {
    (inited: any): void;
    waite(): void;
};
export function vConsole(): void;
export function getStyle($el: any, name: any): any;
export function cssSetRest($el: any, names: any): {
    set: (vals: any) => void;
    reset: () => void;
};
export function cloneElement(el: any, tag: any): any;
export function objectMerge(to: any, from: any, depth: any): any;
export function keepAlive(inst: any): any;
export function keepAliveClose(key: any): void;
export function keepAliveDel($keepAlive: any, key: any, cKey: any): void;
export function getPaths(model: any, paths: any, fromI: any): any;
export function setPaths(model: any, val: any, paths: any, fromI: any): any;
export function date(time: any): Date;
export function formatTime(time: any, fmt: any): any;
export function formatDate(date: any, fmt: any): any;
export function aName(name: any): string;
export function aNameC(a: any, name: any, underline: any): any;
export function fullScreen($this: any, $el: any, fun: any): () => void;
export function cvsVal(val: any, cHead: any, cEnd: any): any;
export function downloadTxt(txt: any, name: any, bom: any): void;
export function funTimer(fun: any): {
    start: (interval: any) => void;
    stop: () => void;
};
export function loadState(bind: any, path: any, target: any, props: any): boolean;
export function saveState(bind: any, path: any, target: any, props: any): void;
}
export default ab_
