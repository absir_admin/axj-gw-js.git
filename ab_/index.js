var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var _nextT = 0;
var ab_ = {
    _ver: false,
    local: true,
    https: false,
    fn: function (fn) {
        ab_ = Object.assign(ab_, fn);
    },
    header: function () {
    },
    back: function () {
        if (history.length > 1) {
            history.back();
        }
        else {
            window.close();
        }
    },
    push: function (uri, title) {
        location.hash = uri;
    },
    replace: function (uri, title) {
        location.replace(uri);
    },
    open: function (uri, title, alone) {
        window.open(uri);
    },
    get: function (name) {
        var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)');
        var r = window.location.search.substr(1).match(reg);
        return r != null ? unescape(r[2]) : null;
    },
    empty: function (o) {
        if (o) {
            if (typeof (o) === 'object') {
                for (var k in o) {
                    return false;
                }
            }
            else {
                return o.length > 0;
            }
        }
        return true;
    },
    urlRm: function (url, para) {
        var i = url.indexOf('&' + para + '=');
        var f = 0;
        if (i < 0) {
            i = url.indexOf('?' + para + '=');
            f = 1;
        }
        if (i >= 0) {
            var j = url.indexOf('&', i + 1);
            if (j > i) {
                if (f) {
                    url = url.substr(0, i + 1) + url.substr(j + 1);
                }
                else {
                    url = url.substr(0, i) + url.substr(j);
                }
            }
            else {
                url = url.substr(0, i);
            }
        }
        return url;
    },
    urlParas: function (url, paras, force) {
        if (typeof (paras) === 'object') {
            var s = '';
            for (var k in paras) {
                var v = paras[k];
                if (v || force) {
                    s += (s ? '&' : '') + k + '=' + encodeURIComponent(v);
                }
            }
            paras = s;
        }
        if (url === undefined || url === null) {
            return paras;
        }
        var i = url.indexOf('?');
        if (i >= 0) {
            return url.substr(0, i + 1) + paras + '&' + url.substr(i + 1);
        }
        else {
            return url + '?' + paras;
        }
    },
    urlPath: function () {
        return location.protocol + '//' + location.host + location.pathname;
    },
    pathDir: function (path) {
        var i = path && path.lastIndexOf('/');
        return i ? path.substr(0, i + 1) : '/';
    },
    route: function (anch, url, set) {
        var hash;
        if (url) {
            hash = url.replace(/^[#]*[\/]*/, '');
        }
        else {
            hash = location.hash;
            hash = hash.replace(/^#[\/]*/, '');
        }
        var href = '/' + hash;
        var $route = ab_.$route;
        if ($route && $route.hash === href) {
            return $route;
        }
        $route = {
            hash: href,
            path: [],
            search: {}
        };
        hash = hash.replace(/([^#])(#.*$)/, '$1');
        if (anch) {
            hash = hash.split('/') || [];
            var len = hash.length;
            for (var i = 0; i < len; i++) {
                var path = hash[i];
                var id = path.indexOf('=');
                if (id >= 0) {
                    $route.search[path.substr(0, id)] = path.substr(id + 1);
                }
                else {
                    $route.path.push(path);
                }
            }
            $route.href = $route.path.join('/');
        }
        else {
            var id = hash.indexOf('?');
            var path = id >= 0 ? hash.substr(0, id) : hash;
            $route.href = path;
            var paths = path.split('/') || [];
            var len = paths.length;
            for (var i = 0; i < len; i++) {
                $route.path.push(paths[i]);
            }
            paths = id >= 0 ? hash.substr(id) : '';
            if (paths && paths[0] === '?') {
                paths = paths.substr(1);
            }
            var search = location.search;
            if (search && search[0] === '?') {
                search = search.substr(1);
            }
            if (search) {
                paths = paths ? (paths + '&' + search) : search;
            }
            $route.searchS = paths;
            if (paths && paths.length > 1) {
                paths = paths.split('&') || [];
                len = paths.length;
                for (var i = 0; i < len; i++) {
                    path = paths[i];
                    id = path.indexOf('=');
                    if (id >= 0) {
                        $route.search[decodeURIComponent(path.substr(0, id))] = decodeURIComponent(path.substr(id + 1));
                    }
                    else {
                        $route.search[decodeURIComponent(path)] = '';
                    }
                }
            }
        }
        if (url === undefined || set) {
            ab_.$route = $route;
        }
        return $route;
    },
    el: function (el) {
        return el || ab_.$el || (ab_.$el = document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] || document);
    },
    ver: function (url) {
        if (ab_._ver && url) {
            url = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'v=' + ab_._ver;
        }
        return url;
    },
    reqCss: function (href, el, media) {
        var link = document.createElement('link');
        link.href = ab_.ver(href);
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.media = media || 'all';
        ab_.el(el).appendChild(link);
    },
    call: function () {
        var call = ab_.$call;
        if (call) {
            delete ab_.$call;
            call.apply(this, arguments);
        }
    },
    requires: function (requires, fun) {
        if (!requires) {
            return;
        }
        if (requires && requires.webpack) {
            requires.keys().forEach(function (key) {
                fun(key, requires(key));
            });
        }
        else {
            for (var key in requires) {
                fun(key, requires[key]);
            }
        }
    },
    nextT: function () {
        var t = ab_.time();
        if (t < _nextT) {
            t = ++_nextT;
        }
        else {
            _nextT = t;
        }
        return t;
    },
    reqJs: function (js, el, onload, onerror) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        if (call) {
            if (script.readyState !== undefined) {
                //IE
                if (onload || onerror) {
                    script.onreadystatechange = function () {
                        var state = script.readyState;
                        if (state && typeof (state) === 'string') {
                            if (state === 'loaded') {
                                onload && onload();
                            }
                            else if (state.indexOf('err') >= 0 || state.indexOf('fail') >= 0) {
                                onerror && onerror();
                            }
                        }
                        else {
                            onload && onload();
                        }
                    };
                }
            }
            else { //Other
                onload && (script.onload = call);
                onerror && (script.onerror = onerror);
            }
        }
        script.src = ab_.ver(js);
        ab_.el(el).appendChild(script);
    },
    reqModule: function (name, jses, csses) {
        return function (call) {
            if (!call) {
                return;
            }
            if (ab_['_' + name + '_']) {
                call();
                return;
            }
            var calls = ab_['_' + name + '_s'];
            if (calls) {
                calls.push(call);
                return;
            }
            ab_['_' + name + '_s'] = [call];
            if (csses) {
                var len = csses.length;
                for (var i = 0; i < len; i++) {
                    ab_.reqCss(csses[i]);
                }
            }
            var $i = 0;
            var $len = jses ? jses.length : 0;
            function next() {
                if ($i < $len) {
                    var js = jses[$i++];
                    if (typeof (js) === 'function') {
                        try {
                            js = js();
                        }
                        finally {
                            if (js && typeof (js) == 'string') {
                                ab_.reqJs(js, 0, next);
                            }
                            else {
                                next();
                            }
                        }
                    }
                    else {
                        ab_.reqJs(js, 0, next);
                    }
                }
                else {
                    ab_['_' + name + '_'] = 1;
                    var calls = ab_['_' + name + '_s'];
                    if (calls) {
                        delete ab_['_' + name + '_s'];
                        var len = calls.length;
                        for (var i = 0; i < len; i++) {
                            try {
                                calls[i]();
                            }
                            catch (e) {
                                console.error(e);
                            }
                        }
                    }
                }
            }
            next();
        };
    },
    eval: function (exp, $page, $this, res) {
        var fn = eval(exp);
        if (fn && typeof (fn) === 'function') {
            fn();
        }
    },
    $center: {},
    args: function (args, off, len) {
        if (off == undefined) {
            return Array.prototype.slice.call(args);
        }
        if (len === undefined) {
            len = args.length;
        }
        var ary = [];
        for (; off < len; off++) {
            ary.push(args[off]);
        }
        return ary;
    },
    swap: function (ary, from, to) {
        if (ary[from] && ary[to]) {
            ary[from] = ary.splice(to, 1, ary[from])[0];
        }
        return ary;
    },
    reSort: function (ary, from, to) {
        if (from === to) {
            return;
        }
        if (from === to + 1 || from == to - 1) {
            ab_.swap(ary, from, to);
            return;
        }
        var fromE = ary[from];
        ary.splice(from, 1);
        ary.splice(to, 0, fromE);
        return ary;
    },
    resetEl: function (children, fromI, toI) {
        if (toI == fromI) {
            return;
        }
        var to = children[toI];
        var from = children[fromI];
        if (to && from) {
            if (toI < fromI) {
                from.after(to);
            }
            else {
                from.before(to);
            }
        }
    },
    swapEl: function (from, to) {
        var next = from.nextSibling;
        if (next === to) {
            to.after(from);
            return;
        }
        var parent = from.parentElement;
        to.after(from);
        if (next) {
            next.before(to);
        }
        else if (parent) {
            parent.appendChild(to);
        }
    },
    post: function (name) {
        var args = false;
        var from = undefined;
        if (arguments.length >= 3 && arguments[0] === undefined) {
            name = arguments[1];
            args = true;
            from = arguments[3];
        }
        var target = ab_.$center;
        var fn = target[name];
        if (fn) {
            if (from) {
                target['$from'] = from;
            }
            fn.apply(target, args ? arguments[2] : ab_.args(arguments, 1));
            return;
        }
        while (true) {
            var i = name.indexOf('/');
            if (i <= 0) {
                return;
            }
            target = target[name.substr(0, i)];
            if (typeof (target) !== 'object') {
                return;
            }
            name = name.substr(i + 1);
            fn = target[name];
            if (fn) {
                if (from) {
                    target['$from'] = from;
                }
                fn.apply(target, args ? arguments[2] : ab_.args(arguments, 1));
                return;
            }
        }
    },
    posts: function (data, i, from) {
        var last = data.length - 1;
        for (i = i || 0; i < last; i += 2) {
            try {
                ab_.post(undefined, data[i], data[i + 1], from);
            }
            catch (e) {
                console.error(e);
            }
        }
    },
    langs: {},
    langChange: function (lang, call) {
        if (!lang) {
            lang = navigator.language || navigator.userLanguage;
            if (!lang) {
                call(false);
                return;
            }
        }
        ab_.require('./i18n/lang_' + lang + '.json', true, function (data) {
            if (data) {
                ab_.langs = Object.assign(ab_.langs, data);
                call(true);
                return;
            }
            var i = lang.indexOf('-');
            if (i > 0) {
                ab_.langChange(lang.substr(0, i), call);
                return;
            }
            call(false);
        });
    },
    lang: function (key, lang) {
        if (lang) {
            if (!ab_.langs[key]) {
                ab_.langs[key] = lang;
            }
        }
    },
    i18n: function (key, par) {
        var s = ab_.$i18n ? ab_.langs[key] || key : key;
        if (s && (Array.isArray(par) || typeof (par) === 'object')) {
            s = s.replace(/(\\)?\{([^\{\}\\]+)(\\)?\}/g, function (word, slash1, token, slash2) {
                // 如果有转义的\{或\}替换转义字符
                if (slash1 || slash2) {
                    return word.replace('\\', '');
                }
                return par[token] || '';
            });
        }
        return s;
    },
    time: function () {
        return parseInt(new Date().getTime() / 1000);
    },
    humTime: function (time, max) {
        if (time < (max || 1000)) {
            return time + 's';
        }
        if (time < 600) {
            return Math.floor(time / 60) + 'm' + (time % 60) + 's';
        }
        if (time < (max ? max * 60 : 60000)) {
            return Math.ceil(time / 60) + 'm';
        }
        if (time < 36000) {
            return Math.floor(time / 3600) + 'h' + Math.ceil((time % 3600) / 60) + 'm';
        }
        return Math.ceil(time / 3600) + 'h';
    },
    humSize: function (size) {
        var kb = size / 1024;
        if (kb < 10000) {
            return kb.toFixed(2) + 'kb';
        }
        var mb = kb / 1024;
        if (mb < 10000) {
            return mb.toFixed(2) + 'mb';
        }
        var gb = mb / 1024;
        if (gb < 10000) {
            return gb.toFixed(2) + 'gb';
        }
        return (gb / 1024).toFixed(2) + 'tb';
    },
    parseJson: function (json) {
        return typeof (json) === 'string' ? JSON.parse(json) : json;
    },
    toJson: function (obj) {
        return JSON.stringify(obj);
    },
    tplRender: function (tpl, render) {
        // 被转义的的分隔符 { 和 } 不应该被渲染，分隔符与变量之间允许有空白字符
        /*
        "{greeting}! My name is { author.name }.".render({
            greeting: "Hi",
            author: {
            name: "hsfzxjy"
        }});
         */
        return tpl.replace(/(\\)?\{([^\{\}\\]+)(\\)?\}/g, function (word, slash1, token, slash2) {
            // 如果有转义的\{或\}替换转义字符
            if (slash1 || slash2) {
                return word.replace('\\', '');
            }
            if (!render) {
                return '';
            }
            // 切割 token ,实现级联的变量也可以展开
            var tokens = token.replace(/\s/g, '').split('.');
            var len = tokens.length;
            var curr = render;
            for (var i = 0; i < len; ++i) {
                token = tokens[i];
                curr = curr[token];
                // 如果当前索引的对象不存在，则直接返回空字符串。
                if (curr === undefined || curr === null)
                    return '';
            }
            return curr;
        });
    },
    // xhrOntimeout() {
    //     // console.log('请求超时。。')
    // },
    // 支持formData file var form = new FormData();form.append("file", fileObj);
    xhrReq: function (loading, headers, url, posts, callback, init) {
        var xhr = new XMLHttpRequest();
        var done = XMLHttpRequest.DONE || 4;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === done) {
                loading && loading();
                if (callback) {
                    if (xhr.status === 200) {
                        callback(null, xhr.responseText);
                    }
                    else {
                        callback(xhr.text || 'fail', null);
                    }
                }
            }
        };
        // 设置30s超时时间
        // xhr.timeout = 30000
        // xhr.ontimeout = ab_.xhrOntimeout()
        if (posts) {
            xhr.open('POST', url, true);
        }
        else {
            xhr.open('GET', url, true);
            posts = undefined;
        }
        if (headers) {
            for (var name in headers) {
                xhr.setRequestHeader(name, headers[name]);
            }
        }
        if (posts && posts._progress) {
            // onprogress 事件在浏览器下载指定的视频/音频（audio/video）时触发
            // xhr.onprogress = posts._progress
            if (xhr.upload) {
                xhr.upload.addEventListener('progress', posts._progress, false);
            }
            delete posts._progress;
            posts._abort = function () {
                xhr.abort();
            };
        }
        if (posts && posts._init) {
            posts._init(xhr);
            delete posts._init;
        }
        init && init(xhr);
        loading && loading(1);
        xhr.send(posts);
    },
    uniReq: function (loading, headers, url, posts, files, callback, init) {
        var req = {
            url: url
        };
        if (headers) {
            req.header = headers;
        }
        req.success = function (rep) {
            loading && loading();
            if (callback) {
                callback(undefined, rep.data);
            }
        };
        req.fail = function (err) {
            loading && loading();
            if (callback) {
                callback(err || 'fail', undefined);
            }
        };
        init && init(req);
        if (files) {
            var len = files.length;
            if (!len) {
                // 表单提交
                var header = __assign({}, req.header);
                header['content-type'] = 'application/x-www-form-urlencoded';
                req.header = header;
                req.method = 'post';
                req.dataType = 'x-www-form-urlencoded';
                req.data = posts;
                uni.request(req);
                return;
            }
            if (len === 1) {
                // 单文件上传
                var file = files[0];
                req.name = file.name;
                req.filePath = file.filePath;
            }
            else {
                // 多文件上传
                req.files = files;
            }
            if (posts) {
                req.formData = posts;
            }
            var header = __assign({}, req.header);
            header['Content-Type'] = 'multipart/form-data';
            req.header = header;
            uni.uploadFile(req);
        }
        else {
            if (posts !== undefined && posts !== 0) {
                // post请求
                req.method = 'post';
                if (posts !== '') {
                    req.data = posts;
                }
            }
            uni.request(req);
        }
    },
    newReady: function (last) {
        var backs = [];
        function ready(back) {
            if (backs) {
                backs.push(back);
            }
            else {
                back();
            }
        }
        ready.start = function () {
            if (backs) {
                var calls = backs;
                backs = undefined;
                for (var i = 0; i < calls.length; i++) {
                    try {
                        calls[i]();
                    }
                    catch (e) {
                        console.error(e);
                    }
                }
                if (last) {
                    var call = last;
                    last = undefined;
                    try {
                        call();
                    }
                    catch (e) {
                        console.error(e);
                    }
                }
            }
        };
        return ready;
    },
    newStart: function (run) {
        var i = 0;
        function start(inited) {
            if (++i > 0 || inited) {
                if (run != null) {
                    var call = run;
                    run = undefined;
                    try {
                        call();
                    }
                    catch (e) {
                        console.error(e);
                    }
                }
            }
        }
        start.waite = function () {
            i--;
        };
        return start;
    },
    vConsole: function () {
        if (!ab_.vconsoled) {
            ab_.vconsoled = true;
            ab_.reqJs('https://cdn.bootcss.com/vConsole/3.3.4/vconsole.min.js', 0, function () {
                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.appendChild(document.createTextNode('new VConsole()'));
                document.body.appendChild(script);
            });
        }
    },
    getStyle: function ($el, name) {
        var style = $el.getAttribute ? $el.getAttribute('style') : $el.attr('style');
        if (style) {
            var match = style.match(new RegExp(name + '[\\s\\t\\r\\n]*:([^;]*)', 'i'));
            if (match) {
                return match[1];
            }
        }
    },
    cssSetRest: function ($el, names) {
        var len = names.length;
        var css = [];
        for (var i = 0; i < len; i++) {
            css.push(ab_.getStyle($el, names[i]) || '');
        }
        return $el.style ? {
            set: function (vals) {
                for (var i = 0; i < len; i++) {
                    $el.style[names[i]] = vals[i] || '';
                }
            },
            reset: function () {
                for (var i = 0; i < len; i++) {
                    $el.style[names[i]] = css[i];
                }
            }
        } : {
            set: function (vals) {
                for (var i = 0; i < len; i++) {
                    $el.css(names[i], vals[i] || '');
                }
            },
            reset: function () {
                for (var i = 0; i < len; i++) {
                    $el.css(names[i], css[i]);
                }
            }
        };
    },
    cloneElement: function (el, tag) {
        if (el) {
            var clone = document.createElement(tag || el.tagName);
            var attrs = el.attributes;
            for (var i = 0; i < attrs.length; i++) {
                var attr = attrs[i];
                clone.setAttribute(attr.name, attr.value);
            }
            while (el.firstChild) {
                clone.appendChild(el.firstChild);
            }
            return clone;
        }
    },
    objectMerge: function (to, from, depth) {
        to = typeof (to) === 'object' ? to : {};
        for (var k in from) {
            var f = from[k];
            if (depth > 0) {
                if (f && typeof (f) === 'object') {
                    var t = to[k];
                    if (typeof (t) === 'object') {
                        ab_.objectMerge(t, f, depth - 1);
                        continue;
                    }
                }
            }
            to[k] = f;
        }
        return to;
    },
    keepAlive: function (inst) {
        if (!inst) {
            inst = $root.$route.matched;
            if (inst.length) {
                inst = inst[inst.length - 1].instances["default"];
            }
            if (!inst) {
                return;
            }
        }
        inst = inst.$vnode || inst;
        inst = inst.parent;
        return inst && inst.componentInstance;
    },
    keepAliveClose: function (key) {
        var matched = $root.$route.matched;
        if (matched.length > 1) {
            ab_.keepAliveDel(ab_.keepAlive(matched[matched.length - 1].instances["default"]), key);
        }
    },
    keepAliveDel: function ($keepAlive, key, cKey) {
        if ($keepAlive && (key || cKey)) {
            $keepAlive = $keepAlive.componentInstance || $keepAlive;
            var cache = $keepAlive.cache;
            var keys = $keepAlive.keys;
            if (cache) {
                if (!cKey && key) {
                    for (var k in cache) {
                        if (cache[k].data.key === key) {
                            cKey = k;
                            break;
                        }
                    }
                }
                if (cKey) {
                    if (keys.length) {
                        var index = keys.indexOf(cKey);
                        if (index >= 0) {
                            keys.splice(index, 1);
                        }
                    }
                    var c = cache[cKey];
                    if (c) {
                        try {
                            c.componentInstance.$destroy();
                        }
                        catch (e) {
                            console.error(e);
                        }
                    }
                    delete cache[cKey];
                }
            }
        }
    },
    getPaths: function (model, paths, fromI) {
        if (typeof (model) === 'object' && typeof (paths) === 'string') {
            var i = fromI = fromI > 0 ? fromI : 0;
            while ((i = paths.indexOf('.', i)) > 0) {
                var j = paths[i - 1] === ']' ? paths.lastIndexOf('[', i - 1) : -1;
                j = j <= fromI ? -1 : j;
                var path = paths.substr(fromI, (j > 0 ? j : i) - fromI);
                var m = model[path];
                if (m) {
                    if (j > 0) {
                        m = m[paths.substr(j + 1, i - j - 2)];
                    }
                    return ab_.getPaths(m, paths, ++i);
                }
                i++;
            }
            return model[fromI > 0 ? paths.substr(fromI) : paths];
        }
    },
    setPaths: function (model, val, paths, fromI) {
        if (typeof (model) === 'object' && typeof (paths) === 'string') {
            var i = fromI = fromI > 0 ? fromI : 0;
            while ((i = paths.indexOf('.', i)) > 0) {
                var j = paths[i - 1] === ']' ? paths.lastIndexOf('[', i - 1) : -1;
                j = j <= fromI ? -1 : j;
                var path = paths.substr(fromI, (j > 0 ? j : i) - fromI);
                var m = model[path];
                if (m) {
                    if (j > 0) {
                        m = m[paths.substr(j + 1, i - j - 2)];
                    }
                    return ab_.setPaths(m, val, paths, ++i);
                }
                i++;
            }
            model[fromI > 0 ? paths.substr(fromI) : paths] = val;
        }
    },
    date: function (time) {
        return time > 0 ? new Date(time > 2147483647 ? time : (time * 1000)) : null;
    },
    formatTime: function (time, fmt) {
        return ab_.formatDate(ab_.date(time), fmt);
    },
    formatDate: function (date, fmt) {
        if (!date || !date.getFullYear) {
            return '';
        }
        fmt = fmt || 'yyyy-MM-dd hh:mm:ss';
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
        }
        var o = {
            'M+': date.getMonth() + 1,
            'd+': date.getDate(),
            'h+': date.getHours(),
            'm+': date.getMinutes(),
            's+': date.getSeconds()
        };
        for (var k in o) {
            if (new RegExp("(".concat(k, ")")).test(fmt)) {
                var str = o[k] + '';
                fmt = fmt.replace(RegExp.$1, str.length > 1 ? str : ('0' + str));
            }
        }
        return fmt;
    },
    aName: function (name) {
        if (typeof (name) !== 'string') {
            return '';
        }
        var len = name && name.length;
        var i = 0;
        var c;
        for (; i < len; i++) {
            c = name[i];
            if (c < 'a' || c > 'z') {
                break;
            }
        }
        return i >= len ? '' : name.substr(0, i);
    },
    aNameC: function (a, name, underline) {
        if (!a || !name) {
            return name;
        }
        if (underline) {
            return a + '_' + name;
        }
        return (a === '@' ? '' : a) + name[0].toUpperCase() + name.substr(1);
    },
    fullScreen: function ($this, $el, fun) {
        var $parent = $this.parentNode;
        var parentSetRest;
        var thisSetRest;
        var $fixeds;
        return function () {
            if (!parentSetRest) {
                parentSetRest = ab_.cssSetRest($parent, ['position', 'z-index', 'top', 'right', 'bottom', 'left', 'background', 'width', 'height']);
                thisSetRest = ab_.cssSetRest($this, ['position', 'top', 'right', 'bottom', 'left', 'width', 'height']);
            }
            if ($fixeds) {
                parentSetRest.reset();
                thisSetRest.reset();
                for (var i = 0; i < $fixeds.length; i++) {
                    var $fixed = $fixeds[i];
                    if ($fixed[1]) {
                        $fixed[0].attr('style', $fixed[1]);
                    }
                    else {
                        $fixed[0].removeAttr('style');
                    }
                }
                $fixeds = null;
                if ($el && $el.classList) {
                    $el.classList.remove('.abFullScreenReset');
                    $el.classList.add('.abFullScreenTo');
                }
                fun && fun();
            }
            else {
                parentSetRest.set(['fixed', '19891024', '0', '0', '0', '0', '#FFF']);
                thisSetRest.set(['absolute', '0', '0', '0', '0']);
                $fixeds = [];
                $fixed = $parent;
                if ($fixed.style) {
                    while (true) {
                        $fixed = $fixed.parentNode;
                        if (!$fixed || !$fixed.length || $fixed[0] === document) {
                            break;
                        }
                        if ($fixed.style.position === 'fixed') {
                            $fixeds.push([$fixed, $fixed.getAttribute('style')]);
                            $fixed.style.position === 'static';
                        }
                    }
                }
                else {
                    while (true) {
                        $fixed = $fixed.parent();
                        if (!$fixed || !$fixed.length || $fixed[0] === document) {
                            break;
                        }
                        if ($fixed.css('position') === 'fixed') {
                            $fixeds.push([$fixed, $fixed.attr('style')]);
                            $fixed.css('position', 'static');
                        }
                    }
                }
                if ($el && $el.classList) {
                    $el.classList.remove('.abFullScreenTo');
                    $el.classList.add('.abFullScreenReset');
                }
                fun && fun(true);
            }
        };
    },
    cvsVal: function (val, cHead, cEnd) {
        if (cHead || cEnd) {
            var head = 0;
            var last = val.length - 1;
            var cLast = last;
            for (; head <= last; head++) {
                var c = val[head];
                if (c !== '\r' && c !== '\n') {
                    break;
                }
            }
            for (; cLast > head; cLast--) {
                var c = val[cLast];
                if (c !== '\r' && c !== '\n') {
                    break;
                }
            }
            if (head > 0 || cLast < last) {
                val = val.substr(head, cLast + 1 - cHead);
            }
        }
        if (typeof (val) === 'string') {
            if (val.indexOf(',') >= 0 || val.indexOf('"') >= 0 || val.indexOf('\r') >= 0 || val.indexOf('\n') >= 0) {
                val = '"' + val.replace('"', '""') + '"';
            }
        }
        return val === undefined || val === null ? '' : val;
    },
    downloadTxt: function (txt, name, bom) {
        if (bom) {
            txt = '\uFEFF' + txt;
        }
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=' + document.characterSet + ',' + encodeURIComponent(txt));
        element.setAttribute('download', name);
        element.style.display = 'none';
        element.click();
        setTimeout(function () {
            if (element) {
                element.remove();
            }
        }, 100);
    },
    funTimer: function (fun) {
        var timer;
        var timerI;
        var self = {
            start: function (interval) {
                if (timer && timerI != interval) {
                    self.stop();
                }
                if (!timer) {
                    timerI = interval;
                    timer = setInterval(fun, interval);
                }
            },
            stop: function () {
                if (timer) {
                    try {
                        clearInterval(timer);
                    }
                    catch (e) {
                        console.error(e);
                    }
                    timer = null;
                }
            }
        };
        return self;
    },
    loadState: function (bind, path, target, props) {
        var state = bind && bind['_$$state$$' + path];
        if (state && target) {
            var len = props && props.length;
            for (var i = 0; i < len; i++) {
                var prop = props[i];
                target[prop] = state[prop];
            }
            return true;
        }
    },
    saveState: function (bind, path, target, props) {
        if (bind && target) {
            var state = {};
            var len = props && props.length;
            for (var i = 0; i < len; i++) {
                var prop = props[i];
                state[prop] = target[prop];
            }
            bind['_$$state$$' + path] = state;
        }
    }
};
try {
    if (typeof location !== undefined) {
        ab_.local = location.hostname == '127.0.0.1' || location.hostname == 'localhost';
        ab_.https = location.href.startsWith('https');
    }
}
catch (e) {
    console.error(e);
}
try {
    if (globalThis) {
        if (globalThis.ab_) {
            ab_ = globalThis.ab_;
        }
        else {
            globalThis.ab_ = ab_;
        }
    }
}
catch (e) {
    console.error(e);
}
module.exports = ab_;
