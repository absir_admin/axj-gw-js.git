#!/usr/bin/env bash
cd `dirname $0`

tsc --declaration --allowJs index.js

dec="declare namespace ab_{"
sed -i "1i $dec" index.d.ts
echo '}' >> index.d.ts
echo 'export default ab_' >> index.d.ts